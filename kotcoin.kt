//Defining how blocks should look like

class Block(val index: Int,
            val previousHash: String,
            val data: Any) {
    val hash = calculateHash()
    val timestamp: Long = Date().time
    fun calculateHash(): String {
        val input = (index.toString() + previousHash + timestamp + data).toByteArray()
        return DigestUtils.sha256Hex(input)
    }
}

//actual chain

object Blockchain {
    val chain = mutableListOf<Block>()
    val latestBlock: Block
        get() = chain.last()
    init {
        chain.add(Block(0, "0", "Genesis block"))
    }
    fun addNewBlock(block: Block) {
        if (isNewBlockValid(block)) chain.add(block)
    }
}

fun main(args: Array<String>) {
    val kotcoin = Blockchain
    for (i in 1..15) {
        kotcoin.addNewBlock("Block $i")
    }
    for (block in kotcoin.chain) {
        println("""Data: ${block.data}
            |Previous hash: ${block.previousHash}
            |Current hash: ${block.hash}
        |""".trimMargin())
    }
}
